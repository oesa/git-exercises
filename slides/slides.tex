\documentclass[dvipsnames]{beamer}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}
\usepackage{hyperref}

\usepackage{tikz}
\usetikzlibrary{shapes,positioning}

\mode<presentation>
{
\usetheme{Pittsburgh}
}
\AtBeginSection[]
{
\begin{frame}{Outline}

\tableofcontents[currentsection]

\end{frame}
}

% \setbeameroption{show notes}

% \usepackage{pgfpages}
% \pgfpagesuselayout{4 on 1}[a4paper,landscape,border shrink=5mm]

\title{Git workshop}
\author{EcoEpi}

\newcommand{\exercises}
{
\begin{frame}

\centering

{\Huge Exercise}

\vskip1em

\url{https://oesa.pages.ufz.de/git-exercises/}

\end{frame}
}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}{Outline}

\tableofcontents

\end{frame}

\section{Preparation}

\begin{frame}{Required software}

\begin{itemize}

\item Git from \url{https://git-scm.com/downloads}

\item a text editor, for example Notepad++ from \url{https://notepad-plus-plus.org/downloads}

\item Pandoc from \url{https://pandoc.org/installing.html} (only required to complete some of the exercises)

\item request access to GitLab for your UFZ account at \url{https://git.ufz.de} (can take a bit of time as WKDV needs to approve)

\end{itemize}

\end{frame}

\begin{frame}{Procedure}

\begin{itemize}

\item sections on concepts will be interleaved with exercises

\item there is a short break after each exercise

\item every participant will create his/her own Git repository and GitLab project

\item if anybody needs help during the exercises, we will use breakout rooms and screen sharing

\item signal for help, if you have question or if we should slow down via the chat

\end{itemize}

\end{frame}

\note[itemize]
{
	
\item we will use the CLI because it exposes the terms and concepts directly, even though more usable TUI and GUI frontends exist (if you know how to use the CLI, you should be able to understand any TUI/GUI)

\item use two browser windows, one for the meeting and one for the exercises

\item if you have two monitors, we recommend to put the presentation on one and perform the exercise on the other

}

\section{Introduction}

\begin{frame}{Cautionary tale}

\includegraphics[width=\textwidth]{xkcd}

\end{frame}

\note[itemize]
{
\item Git was originally conceived to manage the code of the Linux kernel and hence targeted trained developers and therefore did not refrain from exposing its internals

\item it also has a Unix background and brings some conventions like dot files and line endings into a Windows environment

\item just one example of a VCS, not necessarily has the best user experience, consider for example Mercurial and Subversion, but for better or worse is a de facto standard
}

\begin{frame}{Benefits}

\begin{itemize}

\item version control is "Versionssteuerung", not "Versionskontrolle"

\item helps humans \emph{manage large repositories of documents} that are changed often or by multiple collaborators

\item does not limit changes, but tracks them and makes them \emph{reversible} at any time

\item provides a \emph{meta layer} to comment on why changes were made to collaborators including our future selves

\end{itemize}

\end{frame}

\note[itemize]
{
\item use cases for having the complete version history: understand how errors were introduced, experiment without fear of breaking things, refer to specific versions in publications

\item implementations are usually optimized for plain text, but can be used for all kinds of files, not just source code
}

\section{Linear work flow}

\begin{frame}{Repository structure}
\begin{columns}

\begin{column}{0.4\textwidth}
\begin{itemize}

\item version history is a graph of commits beginning at branches

\item each commit is a full snapshot of the repository content

\item commits usually have one or two parents

\end{itemize}
\end{column}

\begin{column}{0.6\textwidth}
\centering
\begin{tikzpicture}[->,node distance=4em]

\tikzstyle{commit} = [draw,ellipse,align=center,minimum width=7em]
\tikzstyle{branch} = [draw,diamond]

\node[branch] (branch-1) {main};
\node[commit] (commit-3) [right=of branch-1] {third commit};
\node[commit] (commit-2) [below=of commit-3] {second commit};
\node[commit] (commit-1) [below=of commit-2] {first commit};

\path
    (commit-2) edge node [sloped,above] {parent} (commit-1)
    (commit-3) edge node [sloped,above] {parent} (commit-2)
    (branch-1) edge [dash dot] node [sloped,above] {head} (commit-3);

\draw[->] (0,-3) -- (0,-2) node[midway,right] {$t$};

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\note[itemize]
{
\item use cases for having whole-repository snapshots: multiple modules with changing interfaces between them, a document composed of multiple chapters and/or figures, a simulation model and its accompanying documentation

\item note that parent pointers point into the past, i.e. the history graph is traversed from present to past

\item history is considered immutable, i.e. a snapshot of the full repository content and all previous changes that led there
}

\begin{frame}{Making history}
\begin{columns}
    
\begin{column}{0.4\textwidth}
\begin{itemize}

\item three-pronged approach
\begin{itemize}
\item working copy
\item staging area
\item repository
\end{itemize}

\item working copy contains \emph{plain files} that one can access as usual

\item new or modified files are staged \emph{incrementally}

\item staged changes are committed to the repository \emph{as a unit}

\end{itemize}
\end{column}

\begin{column}{0.6\textwidth}
\centering
\begin{tikzpicture}[->]

\node (repository) [draw,minimum height=5em] {repository (committed)};
\node (staging-area) [draw,below=of repository] {staging area ({\color{Green} staged})};
\node (working-copy) [draw,below=of staging-area,minimum height=3em] {working copy ({\color{Red} modified, untracked})};

\path
    (working-copy) edge [bend left] node [left] {add} (staging-area)
    (staging-area) edge [bend left] node [left] {commit} (repository)
    (staging-area) edge [bend left] node [right] {reset} (working-copy)
    (repository) edge [bend left] node [right] {remove} (staging-area);

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\exercises

\section{Non-linear work flow}

\begin{frame}{Branches}
\begin{columns}
    
\begin{column}{0.5\textwidth}
\begin{itemize}

\item branches are entry points to the history stored in the repository

\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\centering
\begin{tikzpicture}[->]

\node (c-1) at (0,0) [circle,fill] {};
\node (c-2) at (0,2) [circle,fill] {};
\node (m) at (0,4) [draw,thick,diamond] {main};

\path
    (m) edge [thick,dash dot] node {} (c-2)
    (c-2) edge [thick] node {} (c-1);

\draw[->] (-1,1.5) -- (-1,2.5) node[midway,right] {$t$};

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\begin{frame}{Branches}
\begin{columns}
  
\begin{column}{0.5\textwidth}
\begin{itemize}

\item they reduce context and allow parallel lines of development

\item are cheap with Git which suggests using them often

\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\centering
\begin{tikzpicture}[->,scale=0.7]

\node (c-1) at (0,0) [circle,fill] {};
\node (c-2) at (0,2) [circle,fill] {};
\node (c-3) at (0,5) [circle,fill] {};
\node (c-5) at (3,4) [circle,fill] {};
\node (c-6) at (3,6) [circle,fill] {};
\node (m) at (0,8) [draw,diamond] {main};
\node (t) at (3,8) [draw,diamond] {topic};

\path
    (m) edge [thick,dash dot] node {} (c-3)
    (c-3) edge [thick] node [below,sloped] {\tiny parent} (c-2)
    (c-2) edge [thick] node {} (c-1)
    (t) edge [thick,dash dot] node {} (c-6)
    (c-6) edge [thick] node {} (c-5)
    (c-5) edge [thick] node [below,sloped] {\tiny parent} (c-2);

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\note[itemize]
{
\item examples for creating branches are: adding a submodel, trying a new algorithm, fixing a mistake

\item reducing context means reducing the amount of things you have to keep in mind at any given time

\item branches are pointers, not the set of commits reachable via the pointer, i.e. branches can be deleted without loosing the associated commits if they are reachable via another branch
}

\begin{frame}{Merges}
\begin{columns}
  
\begin{column}{0.5\textwidth}
\begin{itemize}

\item merges fold all changes from one branch into another

\item merges \tikz{\fill[Green] circle(1ex);} are based on three-way comparisons of branch tips \tikz{\fill[Yellow] circle(1ex);} with first common ancestor \tikz{\fill[Red] circle(1ex);}

\item if files have overlapping changes on both branches, conflicts must be resolved manually

\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\centering
\begin{tikzpicture}[->,scale=0.5]

\node (c-1) at (0,0) [circle,fill] {};
\node (c-2) at (0,2) [circle,fill,Red] {};
\node (c-3) at (0,5) [circle,fill,Yellow] {};
\node (c-5) at (4,4) [circle,fill] {};
\node (c-6) at (4,6) [circle,fill,Yellow] {};
\node (c-7) at (0,8) [circle,fill,Green] {};
\node (m) at (0,12) [draw,diamond] {main};
\node (t) at (4,12) [draw,diamond] {topic};

\path
    (m) edge [thick,dash dot] node {} (c-7)
    (c-7) edge [thick] node [below,sloped] {\tiny parent} (c-3)
    (c-3) edge [thick] node {} (c-2)
    (c-2) edge [thick] node {} (c-1)
    (t) edge [thick,dash dot] node {} (c-6)
    (c-7) edge [thick] node [below,sloped] {\tiny parent} (c-6)
    (c-6) edge [thick] node {} (c-5)
    (c-5) edge [thick] node {} (c-2)
    (c-3) edge [bend right,dashed] node [below,sloped] {\tiny diff} (c-2)
    (c-6) edge [dashed] node [below,sloped] {\tiny diff} (c-2);

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\note[itemize]
{
\item note that the merge commit now has two parents whereas other commits have only one

\item conflict resolution is recorded in merge commits, i.e. they are not empty

\item the source branch can usually be deleted after a merge as the commits are reachable via the destination branch

\item if one branch is a linear continuation of the other, Git performs so-called fast-forward merges without a merge commit by default
}

\exercises

\section{Git internals}

\begin{frame}{Problem statement}
\begin{columns}

\begin{column}{0.5\textwidth}
\begin{itemize}

\item need to efficiently track the complete history of multiple documents

\item the steps of this history should be consistent w.r.t. relations between versions of these documents

\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\begin{tikzpicture}[->,scale=0.7,every node/.style={scale=0.7}]

\node (model) at (1,3) [draw,rectangle split,rectangle split parts=2,align=left] {model.nlogo\nodepart{two}\texttt{\_\_includes [ "sheep.nls", "wolf.nls" ]}\\\dots\\\texttt{to go}\\\hskip1em\texttt{ask sheep [ graze ]}\\\hskip1em\texttt{ask wolves [ hunt ]}\\\texttt{end}};

\node (sheep) at (0,0) [draw,rectangle split,rectangle split parts=2,align=left] {sheep.nls\nodepart{two}\texttt{to graze}\\\hskip1em\dots\\\texttt{end}};

\node (wolf) at (2,0) [draw,rectangle split,rectangle split parts=2,align=left] {wolf.nls\nodepart{two}\texttt{to hunt}\\\hskip1em\dots\\\texttt{end}};

\path
    (model) edge node {} (sheep)
    (model) edge node {} (wolf);

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\note[itemize]
{

\item for example, if software is comprised of multiple modules, these modules should have consistent interfaces in each commit

}

\begin{frame}{Deduplication}
\begin{columns}

\begin{column}{0.3\textwidth}
\begin{itemize}

\item content-addressable file system

\item three fundamental types of objects
\begin{itemize}
\item blobs
\item trees
\item commits
\end{itemize}

\item organised as Merkle tree ensuring integrity

\end{itemize}
\end{column}

\begin{column}{0.7\textwidth}
\centering
\begin{tikzpicture}[->,ampersand replacement=\&,scale=0.5,every node/.style={scale=0.5}]

\tikzstyle{blob} = [draw,fill=white,rectangle,align=left,minimum width=9em]
\tikzstyle{tree} = [draw,fill=white,rectangle,align=left,minimum width=9em]
\tikzstyle{commit} = [draw,ellipse,align=center,minimum width=11em]
\tikzstyle{branch} = [draw,diamond]

\pgfdeclarelayer{arrows}
\pgfsetlayers{arrows,main}

\matrix (grid) [row sep=2em,column sep=2em]
{
\node (commits) {Commits}; \&
\node (trees) {Trees}; \&
\node (blobs) {Blobs}; \\
\node[branch] (branch-1) {main}; \\
\node[commit] (commit-3) {extend\\sheep submodel}; \&
\node[tree,draw=Blue] (tree-3) {\texttt{sheep.nls}\\\texttt{wolf.nls}\\\texttt{model.nlogo}}; \&
\node[blob] (sheep-2) {\texttt{to graze\dots}\\\texttt{to reproduce\dots}}; \\
\node[commit] (commit-2) {add\\wolf submodel}; \&
\node[tree,draw=Green] (tree-2) {\texttt{wolf.nls}\\\texttt{sheep.nls}\\\texttt{model.nlogo}}; \&
\node[blob] (wolf-1) {\texttt{to hunt\dots}}; \\
\node[commit] (commit-1) {initial\\model version}; \&
\node[tree,draw=Red] (tree-1) {\texttt{sheep.nls}\\\texttt{model.nlogo}}; \&
\node[blob] (sheep-1) {\texttt{to graze\dots}};
\node[blob] (model-1) [below=of sheep-1] {\texttt{to go\dots}}; \\
};

\begin{pgfonlayer}{arrows}

\path
    (tree-1) edge [Red] node[above,sloped] {\tiny\texttt{2c16015}} (model-1)
    (tree-1) edge [Red] node[above,sloped] {\tiny\texttt{3c7128e}} (sheep-1)
    (tree-2) edge [Green] node[above,sloped,pos=0.2] {\tiny\texttt{2c16015}} (model-1)
    (tree-2) edge [Green] node[above,sloped,pos=0.7] {\tiny\texttt{3c7128e}} (sheep-1)
    (tree-2) edge [Green] node[above,sloped] {\tiny\texttt{5cbee96}} (wolf-1)
    (tree-3) edge [Blue] node[above,sloped,pos=0.1] {\tiny\texttt{2c16015}} (model-1)
    (tree-3) edge [Blue] node[above,sloped] {\tiny\texttt{4073271}} (sheep-2)
    (tree-3) edge [Blue] node[above,sloped] {\tiny\texttt{5cbee96}} (wolf-1)
    (commit-1) edge node[above,sloped] {\tiny\texttt{f59bc8c}} (tree-1)
    (commit-2) edge node[above,sloped] {\tiny\texttt{0b70a90}} (tree-2)
    (commit-3) edge node[above,sloped] {\tiny\texttt{25d7c9b}} (tree-3)
    (commit-2) edge node[above,sloped] {\tiny\texttt{a4f0ba8}} (commit-1)
    (commit-3) edge node[above,sloped] {\tiny\texttt{da8236d}} (commit-2)
    (branch-1) edge [dash dot] node[above,sloped] {\tiny\texttt{6fe82d7}} (commit-3);

\end{pgfonlayer}

\end{tikzpicture}
\end{column}

\end{columns}
\end{frame}

\note[itemize]
{
\item everything is stored as an object, each object can be retrieved based on the hash of its content, this automatically ensures deduplication

\item a hash is a fingerprint, i.e. a small value identifying a large value with the smallest possible probability of collision

\item content-addressable and Merkle tree combine nicely and together imply that each commit in a way contains all of its history
}

\section{Sharing changes}

\begin{frame}{Remotes}

\begin{itemize}

\item Git is distributed, i.e. it transfers full history between machines, i.e. all \emph{clones} are equally complete

\item Git knows other machines holding a copy as remotes

\item branches/commits are transferred from\slash to remotes using \emph{push\slash pull}

\end{itemize}

\begin{center}
\begin{tikzpicture}

\tikzset{
  every node/.style={execute at begin node=\setlength{\baselineskip}{0.75em}}
}

\node (alice) at (1,0) [draw,align=left] {Alice\\\tiny\texttt{bob git.ufz.de/bob/project.git}\\\tiny \texttt{eve gitlab.com/eve/project.git}};
\node (bob) at (4,2) [draw,align=left] {Bob\\\tiny\texttt{alice gitlab.hzdr.de/alice/project.git}\\\tiny\texttt{eve gitlab.com/eve/project.git}};
\node (eve) at (7,0) [draw,align=left] {Eve\\\tiny\texttt{alice gitlab.hzdr.de/alice/project.git}\\\tiny\texttt{bob git.ufz.de/bob/project.git}};

\path
    (alice) edge [<->] node {} (bob)
    (alice) edge [<->] node {} (eve)
    (bob) edge [<->] node {} (eve);

\end{tikzpicture}
\end{center}

\end{frame}

\begin{frame}{Remotes}

\begin{itemize}

\item Git is distributed, i.e. it transfers full history between machines, i.e. all \emph{clones} are equally complete

\item Git knows other machines holding a copy as \emph{remotes}

\item branches/commits are transferred from\slash to remotes using \emph{push\slash pull}

\item but usually, one uses a single preferred repository, e.g. managed as a GitLab project

\end{itemize}

\begin{center}
\begin{tikzpicture}

\tikzset{
  every node/.style={execute at begin node=\setlength{\baselineskip}{0.75em}}
}

\node (gitlab) at (4,2) [draw,thick] {GitLab};
\node (bob) at (4,1) [draw,align=left] {Bob\\\tiny\texttt{origin git.ufz.de/group/project.git}};
\node (alice) at (1,0) [draw,align=left] {Alice\\\tiny\texttt{origin git.ufz.de/group/project.git}};
\node (eve) at (7,0) [draw,align=left] {Eve\\\tiny\texttt{origin git.ufz.de/group/project.git}};

\path
    (bob) edge [->] node {} (gitlab)
    (alice) edge [->,bend left=40] node {} (gitlab)
    (eve) edge [->,bend right=40] node {} (gitlab);

\end{tikzpicture}
\end{center}

\end{frame}

\exercises

\section{Collaboration using GitLab}

\begin{frame}{Features}
\begin{itemize}

\item browse repositories, view and edit files

\item track issues using Kanban boards

\item discuss changes using \emph{merge requests}
\begin{itemize}
\item propose changes to be merged
\item ask questions, make suggestions
\end{itemize}

\item CI/CD, Wiki, pages, containers, packages, \dots

\end{itemize}
\end{frame}

\note[itemize]
{
\item repositories are for learning, issues are for planning and merge requests are for doing

\item GitLab renders Markdown files which is often used to make projects self-documenting

\item the terminology between GitLab and GitHub differs slightly, i.e. merge requests and pull requests are the same thing
}

\exercises

\section{More usage patterns}

\begin{frame}{Ignore generated files}
\begin{itemize}

\item files which can always be generated based on the sources are usually not tracked by version control, for example rendered documents or executable binaries

\item their changes are not interesting to human readers as they are fully dependent on the source changes

\item they also increase the amount of data that has to be managed

\item Git uses \texttt{.gitignore} files to automatically avoid tracking such files based on naming patterns

\end{itemize}
\end{frame}

\exercises

\begin{frame}{Name versions using tags}
\begin{itemize}

\item a tag is a name for a fixed commit, e.g. \texttt{v0.1.5} refers to \texttt{f3c5a77}

\item usually applied to refer to historic versions of the repository

\item for example, the version of a model that matches a publication

\end{itemize}
\end{frame}

\note[itemize]
{
\item tags are not transferred via push/pull by default, must be requested via \texttt{git push --tags}
}

\exercises

\begin{frame}{Keep branches up-to-date}
\begin{itemize}

\item when work on multiple branches progresses in parallel, some branches can fall behind the main branch

\item updating a topic branch can be useful...
\begin{itemize}
\item ...if it requires changes introduced to the main branch
\item ...to proactively resolve merge conflicts
\end{itemize}

\item updating can be done by \emph{merging the main branch back} into the topic branch

\end{itemize}
\end{frame}

\note[itemize]
{
\item \texttt{git rebase} is an alternative option but more complicated and possibly destructive
}

\exercises

\begin{frame}{Commit messages}
\begin{itemize}

\item should talk about \emph{the why instead of the what}

\item should be at least a single proper sentence

\item more involved recommendations exist, c.f. \url{https://chris.beams.io/posts/git-commit/}

\end{itemize}
\end{frame}

\note[itemize]
{
\item the what is already conveyed by the diff

\item these messages are very important to enable "Git archaeology" using \texttt{git log} and \texttt{git blame}
}

\begin{frame}{Branching models}
\begin{itemize}

\item Git does not impose any conventions on how branches are used

\item we only used topic branches so far, and they are often sufficient

\item \emph{git-flow} described at {\small \url{https://nvie.com/posts/a-successful-git-branching-model/}} is a popular choice

\item further options are discussed at {\small \url{https://martinfowler.com/articles/branching-patterns.html}}

\end{itemize}
\end{frame}

\end{document}