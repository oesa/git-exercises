# Exercise 3 - Sharing changes

In this exercise, you will learn how to work with a remote repository. You will create a GitLab project, push your project to it and pull changes from the remote to your local project.

[[_TOC_]]

## Create a GitLab project

1. Log into your GitLab account
2. Click the `+` button in the middle of the top bar and select `New project`
3. Select `Create blank project`
4. Fill the text field `Project name` with `git-workshop`
5. Leave everything else as is and click `Create project` at the bottom of the page

You now have an empty private project. However, there are some useful instructions how to proceed. Only the last section is relevant for us (Push an existing Git repository, partially). Settings of the first sections were made in the first exercise (Git global setup).

## Push your repository

From the last section (Push an existing Git repository), we ignore the first two lines. We set the remote for our repository

```plaintext
git remote add origin https://git.ufz.de/<you>/git-workshop.git
```

Finally, we push our branch `main` to the remote repository

```plaintext
git push --set-upstream origin main
```

<details><summary>Output</summary>

```plaintext
Enumerating objects: 19, done.
Counting objects: 100% (19/19), done.
Delta compression using up to 8 threads
Compressing objects: 100% (17/17), done.
Writing objects: 100% (19/19), 2.68 KiB | 915.00 KiB/s, done.
Total 19 (delta 4), reused 0 (delta 0), pack-reused 0
To https://git.ufz.de/mlange/git-workshop.git
 * [new branch]      main -> main
Branch 'main' set up to track remote branch 'main' from 'origin'.
```
</details>

At this point, Git will ask for your login and password. Use your normal UFZ login here.

When completed, refresh your project's page. You will see some statistics, a list of your project files, and a nicely rendered version of `README.md`.

Finally, we check the graph

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
*   fc527c5 (HEAD -> main, origin/main) Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Notice that there is now a branch `origin/main`, in addition to `main`. Both point to the same commit.

## Pull changes

We will simulate changes made by others by editing file `README.md` online.

In the file list, click `README.md`. Click the blue `edit` button above the text. Make a change to the file, e.g. by adding a brief description

```
# Git Workshop

Notes taken in the OESA Git workshop.

Content

* [Linear workflow](linear-workflow.md)
* [Inspecting](inspecting.md)
* [Amending commits](amending.md)
```

At the top of the page, there is a Preview tab where you can check that your changes render as expected.

Scroll to the bottom of the page and click `Commit changes` (you may want to change the commit message, too).

To make Git notice remote changes, we need to "fetch"

```plaintext
git fetch
```

<details><summary>Output</summary>

```plaintext
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 457 bytes | 57.00 KiB/s, done.
From https://git.ufz.de/.../git-workshop
   fc527c5..f39b285  main       -> origin/main
```
</details>

Check the graph again to see what happened

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* f39b285 (origin/main) Update README.md
*   fc527c5 (HEAD -> main) Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Branch `origin/main` is now one commit ahead of our local `main`. To update the local branch, we need to pull

```plaintext
git pull
```

<details><summary>Output</summary>

```plaintext
Updating fc527c5..f39b285
Fast-forward
 README.md | 2 ++
 1 file changed, 2 insertions(+)
```
</details>

Check the graph again

```plaintext
git log --graph --oneline --all
```

<details><summary>Output</summary>

```plaintext
* f39b285 (HEAD -> main, origin/main) Update README.md
*   fc527c5 Merge branch 'inspect-history'
|\
| * eba0f8f added page on inspecting the history
* |   e25d3cc Merge branch 'amending-commits'
|\ \
| |/
|/|
| * 0639ed4 added page on amending commits
|/
* 1860049 added linear workflow
* d8d9072 initial commit
```
</details>

Now, `origin/main` and `main` point to the same commit again.

----

Continue with [Exercise 4](exercise-4.md)
