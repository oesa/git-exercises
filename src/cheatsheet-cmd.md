# CheatSheet Command Line

[[_TOC_]]

<table width="100%">
    <thead>
        <tr>
            <th>Command</th>
            <th>Effect</th>
            <th>Examples</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <code>dir</code>
            </td>
            <td>List directory content</td>
            <td>
                <code>dir</code>
                <br/>
                <code>dir *.md</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>cd &lt;dir&gt;</code>
            </td>
            <td>Navigate into directory</td>
            <td>
                <code>cd Projects</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>cd ..</code>
            </td>
            <td>Navigate to parent directory</td>
            <td/>
        </tr>
        <tr>
            <td>
                <code>mkdir &lt;dir&gt;</code>
                <br/>
                <code>md &lt;dir&gt;</code>
            </td>
            <td>Create a directory</td>
            <td>
                <code>mkdir git-workshop</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>help</code>
                <br />
                <code>help &lt;cmd&gt;</code>
            </td>
            <td>Get help (for a command)</td>
            <td>
                <code>help mkdir</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>&lt;cmd&gt; &gt; &lt;file&gt;</code>
            </td>
            <td>Redirect output to file</td>
            <td>
                <code>dir &gt; out.txt</code>
                <br />
                <code>echo Hello World &gt; out.txt</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>&lt;cmd&gt; &gt;&gt; &lt;file&gt;</code>
            </td>
            <td>Redirect (append) output to file</td>
            <td>
                <code>dir &gt;&gt; out.txt</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>type &lt;file&gt;</code>
            </td>
            <td>Display the content of a file</td>
            <td>
                <code>type README.md</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>start &lt;file&gt;</code>
            </td>
            <td>Start a program or open a file</td>
            <td>
                <code>start notepad</code>
                <br/>
                <code>start README.md</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>Ctrl+C</code>
            </td>
            <td>Exit/abort the current program</td>
            <td/>
        </tr>
        <tr>
            <td>Up/Down Arrows</td>
            <td>Scroll through previous command</td>
            <td/>
        </tr>
    </tbody>
</table>