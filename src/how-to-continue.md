# How to continue

This chapter provides some links and recommendations on how to continue using and learning Git.

[[_TOC_]]

## Further reading

Some reading suggestions to continue with:

* The official [Git Documentation](https://git-scm.com/doc)
* Chris Beams: [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
* Martin Fowler: [Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
* Vincent Driessen: [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) (Git-Flow)

## Recommended GUI clients

For those who prefer to work with a GUI client rather than the command line:

* [Sourcetree](https://www.sourcetreeapp.com/) is a freeware GUI client for Windows & MacOS.
* Some text editors like [Atom](https://atom.io/) and many IDEs like [IntelliJ](https://www.jetbrains.com/idea/) and [Eclipse](https://www.eclipse.org/) have a Git integration or plugin.
* [git-igitt](https://github.com/mlange-42/git-igitt) is a TUI client for browsing the history in the console.

The Git documentation provides an extensive [list of GUI clients](https://git-scm.com/downloads/guis).

## Git forges

Git forges are online platforms where you can host repositories, and get a lot of other features for project management and collaboration.

**UFZ**

Use the [UFZ GitLab](https://git.ufz.de) for your work-related projects. Non-UFZ collaborators can get an account there, just contact the WKDV.

Don't put your personal/hobby projects there, as they will then be owned by the UFZ.

**Other**

The most famous platforms to use for online repositories:

* On [GitLab.com](https://gitlab.com), you get the same features as with the UFZ GitLab instance. They offer a free plan with unlimited public and private repositories.
* [GitHub](https://github.com) is the most famous Git forge out there. If you want to find contributors to your projects, you probably have the best chance there. It has similar features like GitLab, and also offers a free plan.

Both GitHub and GitLab offer all features you need for your projects, from merge requests over issue trackers and wikis to Continuous Integration. For a comparison between GitHub and GitLab, see e.g. [this blog post](https://blog.codegiant.io/gitlab-vs-github-which-one-is-better-2020-d8ec7fb9542c).
