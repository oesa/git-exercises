# CheatSheet Git Commands

[[_TOC_]]

<table width="100%">
    <thead>
        <tr>
            <th>Command</th>
            <th>Effect</th>
            <th>Examples</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <code>init</code>
            </td>
            <td>Initialize a local repo</td>
            <td>
                <code>git init</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>clone</code>
            </td>
            <td>Clone a remote repo</td>
            <td>
                <code>git clone https://git.ufz.de/oesa/git-exercises.git</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>status</code>
            </td>
            <td>View the repo status</td>
            <td>
                <code>git status</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>show</code>
            </td>
            <td>Show different things</td>
            <td>
                <code>git show</code>
                <br/>
                <code>git show v0.1.0</code>
                <br/>
                <code>git show 3cda487</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>diff</code>
            </td>
            <td>View diffs</td>
            <td>
                <code>git diff</code>
                <br/>
                <code>git diff README.md</code>
                <br/>
                <code>git diff 3cda487 c8b0455</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>log</code>
            </td>
            <td>Show the repo's history</td>
            <td>
                <code>git log</code>
                <br/>
                <code>git log --all</code>
                <br/>
                <code>git log --graph --oneline --all</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>add</code>
            </td>
            <td>Stage files</td>
            <td>
                <code>git add README.md</code>
                <br/>
                <code>git add *.md</code>
                <br/>
                <code>git add --all</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>reset</code>
            </td>
            <td>Un-stage files</td>
            <td>
                <code>git reset README.md</code>
                <br/>
                <code>git reset --all</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>commit</code>
            </td>
            <td>Commit staged changes</td>
            <td>
                <code>git commit</code>
                <br/>
                <code>git commit -m &quot;A commit message&quot;</code>
                <br/>
                <code>git commit --amend</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>checkout</code>
            </td>
            <td>Checkout, e.g. branch</td>
            <td>
                <code>git checkout main</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>branch</code>
            </td>
            <td>List, create, delete branches</td>
            <td>
                <code>git branch</code>
                <br/>
                <code>git branch my-feature</code>
                <br/>
                <code>git branch --delete my-feature</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>merge</code>
            </td>
            <td>merge branch into current branch</td>
            <td>
                <code>git merge my-feature</code>
                <br/>
                <code>git merge my-feature --no-ff</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>fetch</code>
            </td>
            <td>Get remote changes</td>
            <td>
                <code>git fetch</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>pull</code>
            </td>
            <td>Pull changes from remote</td>
            <td>
                <code>git pull origin main</code>
                <br/>
                <code>git pull --all</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>push</code>
            </td>
            <td>Push something to remote</td>
            <td>
                <code>git push -u origin main</code>
                <br/>
                <code>git push --tags</code>
            </td>
        </tr>
        <tr>
            <td>
                <code>tag</code>
            </td>
            <td>List and create tags</td>
            <td>
                <code>git tag</code>
                <br/>
                <code>git tag v0.1.0</code>
                <br/>
                <code>git tag v0.1.0 f3c5a77</code>
            </td>
        </tr>
    </tbody>
</table>
