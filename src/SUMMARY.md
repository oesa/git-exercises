# Summary

[Introduction](introduction.md)

- [Exercise 1 - Linear workflow](./exercise-1.md)
- [Exercise 2 - Non-linear workflow](./exercise-2.md)
- [Exercise 3 - Sharing changes](./exercise-3.md)
- [Exercise 4 - Collaborate using GitLab](./exercise-4.md)
- [Exercise 5 - Ignoring files](./exercise-5.md)
- [Exercise 6 - Tags for reproducibility](./exercise-6.md)
- [Exercise 7 - Keep branches up-to-date](./exercise-7.md)

[How to continue](./how-to-continue.md)
[CheatSheet Command Line](./cheatsheet-cmd.md)
[CheatSheet Git Commands](./cheatsheet-git.md)
